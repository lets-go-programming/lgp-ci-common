# RULES 

> Данный репозиторий подключается к основному репозиторию ci-utils по пути, доступном в переменной `TEST_DIR_COMMON` (default - `common/`).

## Как настроить проверку задания
### Общие правила
1. Под каждое практическое задание создаенися директория.
2. Имя директории должно соответствовать паттерну `task-<task-number>`.
3. Если задание содержит поддзадачи - каждая оформляется в виде отдельной дирекории `task-<task-number>-<subtask-number>`

Примеры:
```none
- task-1/
- task-2-1/
- task-2-2/
```

4. Для конфигурации системы проверки используется файл `ci.cfg`. Который оформляется в виде ключ-значения.
   Для конфигурации доступны следующие параметры:

| Наименование      | Значения | Описание значений                                                                                                                                                                                                    |
|-------------------|----------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| BUILD_MODE        | none     | Пропустить данный шаг (автоматических OK)                                                                                                                                                                            |
|                   | make     | Запускается тег `build` из файла `common/task-<task-number>/Makefile`                                                                                                                                                |
|                   | student  | Запускается тег `build` из файла `<student-name>/task-<task-number>/Makefile`<br/> Примечание: mk-файл должны быть в директории студента                                                                             |  
|                   | default  | Рекурсивных обход директорий с заданиями, сборка всех `main.go` файлов в папку `bin/`. Регекс обхода `<student-name>/task-<task-number>/cmd/.../main.go` <br/> Примечание: mk-файл должны быть в директории студента |
| LINT_MODE         | none     | Пропустить данный шаг (автоматических OK)                                                                                                                                                                            |
|                   | default  | Рекурсивный запуск линтеров для каждой директории `<student-name>/task-<task-number>/` измененных в коммите                                                                                                          |
| UNIT_TESTS_MODE   | none     | Пропустить данный шаг (автоматических OK)                                                                                                                                                                            |
|                   | default  | Рекурсивный запуск тестов для каждой директории `<student-name>/task-<task-number>/`                                                                                                                                 |
| CUSTOM_TESTS_MODE | none     | Пропустить данный шаг (автоматических OK)                                                                                                                                                                            |
|                   | default  | Запускается тег `custom`  из файла `common/task-<task-number>/Makefile`                                                                                                                                              |

Пример файла конфигурации `ci.cfg`:
```txt 
# task-4-3/ci.cfg
BUILD_MODE make
LINT_MODE default
UNIT_TESTS_MODE default
CUSTOM_TESTS_MODE none
```

### Шаг сборки (build)
> Для того, чтобы запустить собственный сценарий проверки установить в конфиге `BUILD_MODE` в `make`.

Для того, чтобы сконфигурировать шаг сборки - используется таргет `build` в файле `common/task-<task-number>/Makefile`.

Доступные переменные при работе с таргетом:
- BIN (`$WORKDIR/bin/$task`) - путь до `bin/` директории, в которую необходимо сложить результаты сборки
- DIR (`$WORKDIR/$student/$task`) - путь до файлов c практической работой студента
- STATIC (`${TEST_DIR_COMMON}/{$task}`) - путь до файлов со статикой из папки `common`

*Подробнее: [build.sh](utils/build.sh)*

## Шаг статической проверки (lint)
Конфигурация собственным сценарием не предусмотрена.

*Подробнее: [lint.sh](utils/lint.sh)*

## Шаг статической модульного тестирования (unit-tests)
Для того, чтобы добавить собственные модульные тесты необходимо разместить их в директории `common/task-<task-number>/tests`.
Тестовые файлы должны иметь расширения `.go` и будут запущены с использованием `go test`.

> Важно: тесты при запуске помещаются в директорию `<student-name>/task-<task-number>/tests/ci/` автоматически.
>
> На этапе тестирования директория `<student-name>/task-<task-number>/tests/ci/` копируется полностью.
>
> Запуск `go mod` и подключение необходимых пакетов (например, testify) также происходит автоматически.

Для получения доступа к собранным файлам доступна переменная окружения `BUILD_BIN`, содержащая путь до директории с бинарными файлами задачи.

Для получения доступа к датасетам и другим стат. файлам доступна переменная окружения `STATIC_DIR`, содержащая путь до директории `common/task-<task-number>`.

> Примечание: флаг `BUILD_BIN` содержит абсолютный путь до директории `bin/task-<task-number>/`, в котороую складываются все собранные файлы на этапе build.
### Пример теста с перегрузкой потоков ввода-ввывода
```go
// <student-name>/task-<task-name>/cmd/service/main.go
package main

import (
	"fmt"
)

func main() {
	var a string
	fmt.Scan(&a)
	fmt.Printf(a)
}

```
```go
// common/task-<task-name>/tests/main_test.go
package ci

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"os"
	"os/exec"
	"strings"
	"testing"
)

func TestMyProgram(t *testing.T) {
	expectedOutput := "some"

	command := fmt.Sprintf("%s/service", os.Getenv("BUILD_BIN"))
	cmd := exec.Command(command)
	cmd.Stdin = strings.NewReader(expectedOutput)

	output, err := cmd.CombinedOutput()
	if err != nil {
		t.Fatalf("Failed to execute command: %s", cmd)
	}
	t.Log(string(output))
	assert.Equal(t, expectedOutput, string(output))
}

```
*Подробнее: [unit-tests.sh](utils/unit-tests.sh)*

## Шаг кастомного тестирования / шаг-песочница (custom-tests)
Данный шаг добавлен для более поздний заданий, в которых происходит работа с линтерами, сборкой и так далее.
На данном шаге можно проверить практическую работу, получив доступ ко всем исходникам, выполнив необходимые команды, проверив содерждание файлов и так далее.

Для того, чтобы сконфигурировать шаг кастомного тестирования - используется таргет `custom` в файле `common/task-<task-number>/Makefile`.

Доступные переменные при работе с таргетом:
- BIN (`$WORKDIR/bin/$task`) - путь до `bin/` директории, в которую необходимо сложить результаты сборки
- DIR (`$WORKDIR/$student/$task`) - путь до файлов c практической работой студента
- STATIC (`${TEST_DIR_COMMON}/{$task}`) - путь до файлов со статикой из папки `common`

*Подробнее: [custom-tests.sh](utils/custom-tests.sh)*