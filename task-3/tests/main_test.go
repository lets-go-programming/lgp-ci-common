package ci

import (
	"encoding/json"
	"github.com/stretchr/testify/assert"
	"log"
	"os"
	"os/exec"
	"path"
	"testing"
)

type testValues struct {
	Num  int     `json:"num_code"`
	Char string  `json:"char_code"`
	Val  float64 `json:"value"`
}

func TestMyProgram(t *testing.T) {
	pathToCommon := path.Join(os.Getenv("STATIC_DIR"), "cases")
	cases := []struct {
		name     string
		config   string
		expected string
		output   string
		isError  bool
	}{
		{
			name:     "test_case_1",
			config:   "testdata/1.yaml",
			expected: ".output/1.json",
			output:   path.Join(pathToCommon, "1.json"),
			isError:  false,
		},
		{
			name:     "test_case_2",
			config:   "testdata/2.yaml",
			expected: ".output/2.json",
			output:   path.Join(pathToCommon, "2.json"),
			isError:  false,
		},
		{
			name:     "test_case_3",
			config:   "testdata/3.yaml",
			expected: ".output/3.json",
			output:   path.Join(pathToCommon, "3.json"),
			isError:  false,
		},
		{
			name:     "test_case_4",
			config:   "testdata/4.yaml",
			expected: ".output/4.json",
			output:   path.Join(pathToCommon, "4.json"),
			isError:  false,
		},
		{
			name:     "test_case_5",
			config:   "testdata/5.yaml",
			expected: ".output/5.json",
			output:   path.Join(pathToCommon, "5.json"),
			isError:  false,
		},
		{
			name:    "test_case_6",
			config:  "testdata/6.yaml",
			isError: true,
		},
		{
			name:    "test_case_7",
			config:  "tests/ci/.output/7.yaml",
			isError: true,
		},
	}
	log.Println("prepare dir")
	if err := os.Mkdir(".output", 0777); err != nil {
		t.Fatal(err)
	}
	t.Cleanup(func() {
		os.RemoveAll(".output")
	})

	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			assert := assert.New(t)
			cmd := exec.Command(path.Join(os.Getenv("BUILD_BIN"), "service"), "--config", tt.config)
			err := cmd.Run()
			if tt.isError {
				assert.Error(err)
			} else {
				assert.NoError(err)
				expectedValues := make([]testValues, 0)
				outputValues := make([]testValues, 0)

				expectedFile, err := os.Open(tt.expected)
				if err != nil {
					t.Fatal(err)
				}
				t.Cleanup(func() {
					if err := expectedFile.Close(); err != nil {
						t.Fatal(err)
					}
				})
				if err := json.NewDecoder(expectedFile).Decode(&expectedValues); err != nil {
					t.Fatal(err)
				}

				outputFile, err := os.Open(tt.output)
				if err != nil {
					t.Fatal(err)
				}
				t.Cleanup(func() {
					if err := outputFile.Close(); err != nil {
						t.Fatal(err)
					}
				})
				if err := json.NewDecoder(outputFile).Decode(&outputValues); err != nil {
					t.Fatal(err)
				}
				if len(outputValues) != len(expectedValues) {
					t.Fatal(err)
				}
				for i := 0; i < len(expectedValues); i++ {
					assert.Equal(expectedValues[i].Num, outputValues[i].Num)
					assert.Equal(expectedValues[i].Char, outputValues[i].Char)
					assert.Equal(expectedValues[i].Val, outputValues[i].Val)
				}
			}

		})
	}
}
